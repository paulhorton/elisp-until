;;; until.el ---  until loop macro  -*- lexical-binding: t -*-

;; Copyright (C) 2020,2023 Paul Horton,  (concat [112 97 117 108 104 64 105 115 99 98 46 111 114 103])

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20200126
;; Updated: 20231009
;; Version: 1.0
;; Keywords: until

;;; Commentary:

;; Sometimes English reads more naturally with "until" than with "while".
;;
;; Compare:
;;   + "add water until the cup is full"
;;   - "continue adding water while the cup is not full"
;;
;;  Similarly for code,
;;
;;   + (until done BODY)
;;   - (while (not done) BODY)
;;
;; A small difference, but so common that I think it would be worth it for emacs to provide an until loop.
;; While not then, we can define our own like I do here.

;; Return value:
;; In an online discussion thread
;; Stefan Monnier suggested the true value which terminated execution might be returned.
;;
;; I think returning the true value is potentially a good idea,
;; Giving the construct the feel of a function or an anaphoric while.
;;
;; But on the other hand programmers might not expect to need to think
;; about what an until loop returns.
;;
;; So I decided to use the name until-true for a version of until
;; which returns the terminating true value.

;;; Change Log:

;;    20230319 added do-repeat-while, do-repeat-until

;;    PH20231009 Decided until should indeed return the terminating true value.
;;    PH20241125 Renamed the returning true value version as until-true

;;; Code:



(defmacro until (test &rest body)
  "Shorthand for (while (not TEST) (BODY...))"
  ;; to consider: wrapping in cl-block to allow early exit with cl-return
  ;;
  (declare (indent 1)
           (debug (form &rest form))
           )
  `(while (null ,test)
     ,@body
     ))


(defmacro until-true (test &rest body)
  "Similar to: (while (not TEST) (BODY...));
but returns the terminating true value."
  ;; Returning the termination true value
  ;; follows a suggestion by Stefan Monnier in a 2018 online thread.
  ;;
  ;; to consider: wrapping in cl-block to allow early exit with cl-return
  ;;
  (declare (indent 1)
           (debug (form &rest form))
           )
  (let ((result-var (gensym "until")))
    `(let (,result-var)
       (while (null (setq ,result-var ,test))
         ,@body
         )
       ,result-var
       )))



(defmacro while-then-once (test &rest body)
  "Like (while TEST BODY);
except that when TEST evaluates to false, BODY is executed one last time.
"
  ;; todo. Place this in new file while.el  PH20250108
  (declare (indent 1)
           (debug (form &rest form))
           )
  `(progn
     (while ,test
       ,@body
       )
     ,@body
     ))



(defmacro until-done (&rest body)
  "Repeatedly eval BODY until variable done? is set to a true value.
Return that value.

variable done? is bound inside the loop, initialy with value nil.
Use (setq done? value) to schedule the loop to exit at the
end of the current iteration.
"
  ;; to consider: wrapping in cl-block to allow early exit with cl-return
  (declare
   (debug (&rest form))
   )
  `(let (done?)
     (progn
       (while (not done?)
         ,@body
         )
       done?
       )))


;; These do-repeat-{while|until} functions perhaps belong in their own package?

(defmacro do-repeat-while (test &rest body)
  "Do BODY once (without evaluating TEST); then repeat while TEST is true."
  (declare (indent 1)
           (debug (form &rest form))
           )
  `(progn
     ,@body
     (while  ,test  ,@body)
     ))



(defmacro do-repeat-until (test &rest body)
  "Do BODY once (without evaluating TEST); then repeat while TEST is false."
  (declare (indent 1)
           (debug (form &rest form))
           )
  `(progn
     ,@body
     (while (null ,test) ,@body)
     ))


(defmacro do-repeat-until-true (test &rest body)
  "Do BODY once (without evaluating TEST); then repeat while TEST is false.

The terminating true value is returned."
  (declare (indent 1)
           (debug (form &rest form))
           )
  (let ((result-var (gensym "until")))
    `(let (,result-var)
     ,@body
     (while  (null (setq ,result-var ,test))  ,@body)
     ,result-var)
     ))



(provide 'until)


;;; until.el ends here
