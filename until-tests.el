;;; until-tests.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2023, Paul Horton, All rights reserved.

;; License: GPLv3

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20231010
;; Updated: 20231010
;; Version: 0.0
;; Keywords: until

;;; Commentary:

;;; Change Log:

;;; Code:


(ert-deftest until-true ()
  (should
   (eq
    'pickme
    (let1  L  '(nil nil pickme nil)
      (until-true (pop L))
      )
    )))


(ert-deftest until-done ()
  (should
   (eq
    9
    (let ((i 0))
      (until-done
       (setq i (1+ i))
       (unless (< i 3)
         (setq done? (* i i))
         )))
     )))



;;; until-tests.el ends here
